# -*- coding: utf-8 -*-
"""
Created on Tue Oct 28 13:07:13 2014

@author: pedro-xubuntu
"""
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

s2=np.array([0.0, -3.18, -6.73, -10.64, -13.88, -16.51, -18.73, -19.65, -18.99, -17.18, -14.73, -12.92, -13.37, -15.54, -16.14, -14.86, -12.94, -9.31, -4.42])
s1=np.array([0.0, -1.41, -5.08, -11.0, -16.73, -21.33, -25.31, -28.9, -31.35, -31.98, -31.51, -30.07, -26.94, -22.21, -18.1, -15.07, -12.73, -10.99, -9.54, -8.36, -6.31, -3.2])

s=signal.correlate(s2,s1)
s3=signal.resample(s1,35)
s4=signal.resample(s2,35)

plt.grid(True)
plt.plot(s3)
plt.plot(s4)
plt.show()