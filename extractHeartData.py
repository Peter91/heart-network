import os
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

class heart_cases():

    def __init__(self):

            self.dir=os.getcwd()

            """Next line get in a list the files into directories Normals and HCM Cases"""
            self.normal_Heart_dir=os.listdir(self.dir+'/Normals April')
            self.HCM_Heart_dir=os.listdir(self.dir+'/HCM Cases')

            """Now create the data structure to save datas
                Data Structure:
                Dict( Key-Values):
                    Key: Parameter/Propiertie
                    Values: Dict(Key: Heart Part - Values: Measures of each patient)
            """
            self.HCM_Heart_datas=dict()
            self.normal_Heart_datas=dict()

            self.times=list() #Here I'm goint to save the total sum of the time in a patient measure
            self.normalHeart_referencePoint=list()
            self.HCMHeart_referencePoint=list()
            self.debugPatient=set()
            """Parser all datas of Normal Heart cases"""
            for file in self.normal_Heart_dir: #Iterate over each file

                f=open(self.dir+'/Normals April/'+file,'r') #Open each file
                datas_normal=[i.replace('\n','').split(',') for i in f.readlines()] #Read and parser the file
                parameter=''
                tp=int(datas_normal[11][1])
                self.times.append(float(datas_normal[tp+15][1]))

                """First step, get volumen time serie to get de maximun/minimun in order to
                sort the timeseries putting it on the middle to have 'Gauss Bell'
                """
                index_line=16 #because the first time point star in line 16, 0.00
                vol=list()
                volMyo=list()
                while datas_normal[index_line][0]!='': #To stop after last time-point
                                    vol.append(float(datas_normal[index_line][20])) #Add to the list Volumen
                                    volMyo.append(float(datas_normal[index_line][21])) #Add to the list Volumen Myo
                                    index_line+=1
                vol=signal.resample(vol,34)
                volMyo=signal.resample(volMyo,34)
                indexMax=np.argmax(vol)
                indexMin=np.argmin(vol)
                """Sorting time serie"""
                if abs(vol[indexMax])>=abs(vol[indexMin]):
                    self.normalHeart_referencePoint.append(indexMax)
                    """1st sort  time serie, with MAX in the middle"""
                    if(16-indexMax)>0:
                        vol=np.concatenate((vol[-(16-indexMax):],vol[:-(16-indexMax)]))
                        volMyo=np.concatenate((volMyo[-(16-indexMax):],volMyo[:-(16-indexMax)]))
                    elif(16-indexMax)<0:
                        vol=np.concatenate((vol[abs(16-indexMax):],vol[:abs(16-indexMax)]))
                        volMyo=np.concatenate((volMyo[abs(16-indexMax):],volMyo[:abs(16-indexMax)]))
                else:
                    self.normalHeart_referencePoint.append(indexMin)
                    """1st sort  time serie, with MIN in the middle"""
                    if(16-indexMin)>0:
                        vol=np.concatenate((vol[-(16-indexMin):],vol[:-(16-indexMin)]))
                        volMyo=np.concatenate((volMyo[-(16-indexMin):],volMyo[:-(16-indexMin)]))
                    elif(16-indexMin)<0:
                        vol=np.concatenate((vol[abs(16-indexMin):],vol[:abs(16-indexMin)]))
                        volMyo=np.concatenate((volMyo[abs(16-indexMin):],volMyo[:abs(16-indexMin)]))

                """2nd Save Volumen into the data structure"""
                if 'Volumen' not in self.normal_Heart_datas:
                    self.normal_Heart_datas['Volumen']=[vol]
                else:
                    val=self.normal_Heart_datas['Volumen']
                    val.append(vol)
                    self.normal_Heart_datas['Volumen']=val
                if 'Volumen Myocardio' not in self.normal_Heart_datas:
                    self.normal_Heart_datas['Volumen Myocardio']=[volMyo]
                else:
                    val=self.normal_Heart_datas['Volumen Myocardio']
                    val.append(vol)
                    self.normal_Heart_datas['Volumen Myocardio']=val


                for i,j in enumerate(datas_normal): #Iterate over into the file

                    if j[0] =='# Parameter:': #First stop, to save the parameter name

                        if j[1]=='Torsion_Regional (Lever Average) [deg/cm]' or j[1]=='Torsion_Regional [deg/cm]':
                            parameter='Torsion_Regional [deg/cm]'
                        elif j[1]=='Torsion_Basal (Lever Average) [deg/cm]' or j[1]=='Torsion_Basal [deg/cm]':
                            parameter='Torsion_Basal [deg/cm]'
                        else:
                            parameter=j[1]

                        if parameter not in self.normal_Heart_datas:
                            self.normal_Heart_datas[parameter]=dict()  #The second dict into the data structure Dict: Key(Heart part) - Value(measures)

                    if j[0] == 'Frame': #Second  stop, to save the measures
                        val=self.normal_Heart_datas[parameter] #Get the dict parameter value

                        for p in range(2,18): #Iterate over each heart part( 16 parts)
                            index_line=i
                            measures=list() #To save measures
                            heart_part=datas_normal[i][p].split('_')[-1] #Get heart part
                            index_line+=1 #Next line, start measures

                            while datas_normal[index_line][0]!='': #To stop after last time-point
                                measures.append(float(datas_normal[index_line][p])) #Add to the list the measure of each part
                                index_line+=1
                            measures.remove(0)
                            measures=signal.resample(measures,34)
                            sub=16-self.normalHeart_referencePoint[-1]
                            if sub>0:
                                measures=np.concatenate((measures[-sub:],measures[:-sub]))
                            elif sub<0:
                                measures=np.concatenate((measures[abs(sub):],measures[:abs(sub)]))

                            if heart_part not in val: #Add to the data structure
                                val[heart_part]=[measures] #Get values of heart part and save
                            else: #Get values of heart part and save
                                val_1=val[heart_part]
                                val_1.append(measures)
                                val[heart_part]=val_1

                        self.normal_Heart_datas[parameter]=val #Save into the parameter dict



            """Parser all datas of HCM Heart cases"""
            for file in self.HCM_Heart_dir:

                f=open(self.dir+'/HCM Cases/'+file,'r') #Open each file
                datas_HCM=[i.replace('\n','').split(',') for i in f.readlines()] #Read and parser the file
                parameter=''
                tp=int(datas_HCM[11][1])
                self.times.append(float(datas_HCM[tp+15][1]))

                """First step, get volumen time serie to get de maximun/minimun in order to
                sort the timeseries putting it on the middle to have 'Gauss Bell'
                """
                index_line=16
                vol=list()
                volMyo=list()
                while datas_HCM[index_line][0]!='': #To stop after last time-point
                                    vol.append(float(datas_HCM[index_line][20])) #Add to the list Volumen
                                    volMyo.append(float(datas_HCM[index_line][21])) #Add to the list Volumen Myo
                                    index_line+=1
                vol=signal.resample(vol,34)
                volMyo=signal.resample(volMyo,34)
                indexMax=np.argmax(vol)
                indexMin=np.argmin(vol)

                """Sorting time serie"""
                if abs(vol[indexMax])>=abs(vol[indexMin]):
                    self.HCMHeart_referencePoint.append(indexMax)
                    """1st sort  time serie, with MAX in the middle"""
                    if(16-indexMax)>0:
                        vol=np.concatenate((vol[-(16-indexMax):],vol[:-(16-indexMax)]))
                        volMyo=np.concatenate((volMyo[-(16-indexMax):],volMyo[:-(16-indexMax)]))
                    elif(16-indexMax)<0:
                        vol=np.concatenate((vol[abs(16-indexMax):],vol[:abs(16-indexMax)]))
                        volMyo=np.concatenate((volMyo[abs(16-indexMax):],volMyo[:abs(16-indexMax)]))
                else:
                    self.HCMHeart_referencePoint.append(indexMin)
                    """1st sort  time serie, with MIN in the middle"""
                    if(16-indexMin)>0:
                        vol=np.concatenate((vol[-(16-indexMin):],vol[:-(16-indexMin)]))
                        volMyo=np.concatenate((volMyo[-(16-indexMin):],volMyo[:-(16-indexMin)]))
                    elif(16-indexMin)<0:
                        vol=np.concatenate((vol[abs(16-indexMin):],vol[:abs(16-indexMin)]))
                        volMyo=np.concatenate((volMyo[abs(16-indexMin):],volMyo[:abs(16-indexMin)]))

                """2nd Save into the data structure"""
                if 'Volumen' not in self.HCM_Heart_datas:
                    self.HCM_Heart_datas['Volumen']=[vol]
                else:
                    val=self.HCM_Heart_datas['Volumen']
                    val.append(vol)
                    self.HCM_Heart_datas['Volumen']=val
                if 'Volumen Myocardio' not in self.HCM_Heart_datas:
                    self.HCM_Heart_datas['Volumen Myocardio']=[volMyo]
                else:
                    val=self.HCM_Heart_datas['Volumen Myocardio']
                    val.append(vol)
                    self.HCM_Heart_datas['Volumen Myocardio']=val

                for i,j in enumerate(datas_HCM): #Iterate over into the file

                    if j[0] =='# Parameter:': #First stop, to save the parameter name
                        if j[1]=='Torsion_Regional (Lever Average) [deg/cm]' or j[1]=='Torsion_Regional [deg/cm]':
                            parameter='Torsion_Regional [deg/cm]'
                        elif j[1]=='Torsion_Basal (Lever Average) [deg/cm]' or j[1]=='Torsion_Basal [deg/cm]':
                            parameter='Torsion_Basal [deg/cm]'
                        else:
                            parameter=j[1]

                        if parameter not in self.HCM_Heart_datas:
                            self.HCM_Heart_datas[parameter]=dict()  #The second dict into the data structure Dict: Key(Heart part) - Value(measures)

                    if j[0] == 'Frame': #Second  stop, to save the measures
                        val=self.HCM_Heart_datas[parameter] #Get the dict parameter value
                        for p in range(2,18): #Iterate over each heart part( 16 parts)
                            index_line=i
                            measures=list() #To save measures
                            heart_part=datas_HCM[i][p].split('_')[-1] #Get heart part
                            index_line+=1 #Next line, start measures

                            while datas_HCM[index_line][0]!='': #To stop after last time-point
                                measures.append(float(datas_HCM[index_line][p])) #Add to the list the measure of each part
                                index_line+=1
                            measures.remove(0)
                            measures=signal.resample(measures,34)
                            sub=16-self.HCMHeart_referencePoint[-1]
                            if sub>0:
                                measures=np.concatenate((measures[-sub:],measures[:-sub]))
                            elif sub<0:
                                measures=np.concatenate((measures[abs(sub):],measures[:abs(sub)]))
                            if heart_part not in val: #Add to the data structure
                                val[heart_part]=[measures] #Get values of heart part and save
                            else: #Get values of heart part and save
                                val_1=val[heart_part]
                                val_1.append(measures)
                                val[heart_part]=val_1

                        self.HCM_Heart_datas[parameter]=val #Save into the parameter dict



    def timepoints_normal_cases_datas(self):
        timespoints=list()
        for i in self.normal_Heart_dir: #Iterate over the name of each file
            #f=np.loadtxt(self.dir+'/Normals April/'+i,dtype='S20',delimiter=',')
            f=open(self.dir+'/Normals April/'+i,'r') #Open each file
            f=f.readlines()
            f=int(f[11].replace('\n','').split(',')[1]) #Get time points number and save in timepoints list
            timespoints.append(f)

        return set(timespoints)

    def timepoints_HCM_cases_datas(self):
        timespoints=list()
        for i in self.HCM_Heart_dir: #Iterate over the name of each file
            #f=np.loadtxt(self.dir+'/Normals April/'+i,dtype='S20',delimiter=',')
            f=open(self.dir+'/HCM Cases/'+i,'r') #Open each file
            f=f.readlines()
            f=int(f[11].replace('\n','').split(',')[1]) #Get time points number and save in timepoints list
            timespoints.append(f)


        return set(timespoints)

    def extractDatas_referenceVolMax(self):
        res=list()
        res_norm=list()
        prop=''
        """First Volumen and VolumenMyocardio"""
        #Data structure Dict: key(Parameter/propierties) - Value(Dict: Key(Heart part) - Value(measures))
        v=list()
        for i in self.normal_Heart_datas['Volumen']:
            v.append(i[16])
        for i in self.HCM_Heart_datas['Volumen']:
            v.append(i[16])
        res.append(v)
        """Normalize vector"""
        mx=np.max(v)
        mn=np.min(v)
        v_norm=[(i-mn)/(mx-mn) for i in v]
        res_norm.append(np.array(v_norm))
        prop+='Volume,'
        v=list()
        for i in self.normal_Heart_datas['Volumen Myocardio']:
            v.append(i[16])
        for i in self.HCM_Heart_datas['Volumen Myocardio']:
            v.append(i[16])
        res.append(v)
        """Normalize vector"""
        mx=np.max(v)
        mn=np.min(v)
        v_norm=[(i-mn)/(mx-mn) for i in v]
        res_norm.append(np.array(v_norm))

        prop+='Volumen_Myocardio,'

        """Rest of prop"""
        for i in self.normal_Heart_datas.keys():
            if not(i=='Volumen' or i=='Volumen Myocardio'):
                for heartPart in self.normal_Heart_datas[i].keys():
                    vals=list()

                    for values in self.normal_Heart_datas[i][heartPart]:
                        vals.append(values[16])
                    for values in self.HCM_Heart_datas[i][heartPart]:
                        vals.append(values[16])

                    res.append(np.array(vals))
                    """Normalize vector"""
                    mx=np.max(vals)
                    mn=np.min(vals)
                    vals_norm=[(i-mn)/(mx-mn) for i in vals]
                    res_norm.append(np.array(vals_norm))

                    prop+=i.replace('[mm]','').replace('[deg]','').replace('[deg/cm]','').replace('[%]','').replace('(','').replace(')','').replace(' ','_')+'_'+heartPart+','
        prop+='Class'

        cl=['Normal']*len(self.normal_Heart_datas['Volumen'])+['Hcm']*len(self.HCM_Heart_datas['Volumen'])
        res.append(cl)
        res_norm.append(cl)

        f2_norm=open('HeartValues_ReferenceVolMax.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax.csv',np.transpose(res),delimiter=',',header=prop, comments='',fmt='%s')
        f2_norm=open('HeartValues_ReferenceVolMax_Norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax_Norm.csv',np.transpose(res_norm),delimiter=',',header=prop, comments='',fmt='%s')


    def extractDatas_referenceVolMax_DoingFileByProp(self):
        res=list()
        res_norm=list()
        prop=''
        """First Volumen and VolumenMyocardio"""
        #Data structure Dict: key(Parameter/propierties) - Value(Dict: Key(Heart part) - Value(measures))
        v=list()
        for i in self.normal_Heart_datas['Volumen']:
            v.append(i[16])
        for i in self.HCM_Heart_datas['Volumen']:
            v.append(i[16])
        res.append(v)
        """Normalize vector"""
        mx=np.max(v)
        mn=np.min(v)
        v_norm=[(i-mn)/(mx-mn) for i in v]
        res_norm.append(np.array(v_norm))
        cl=['Normal']*len(self.normal_Heart_datas['Volumen'])+['Hcm']*len(self.HCM_Heart_datas['Volumen'])
        res.append(cl)
        res_norm.append(cl)
        prop+='Volume,'

        """First Volumen and VolumenMyocardio"""
        v=list()
        for i in self.normal_Heart_datas['Volumen Myocardio']:
            v.append(i[16])
        for i in self.HCM_Heart_datas['Volumen Myocardio']:
            v.append(i[16])
        res.append(v)
        """Normalize vector"""
        mx=np.max(v)
        mn=np.min(v)
        v_norm=[(i-mn)/(mx-mn) for i in v]
        res_norm.append(np.array(v_norm))
        prop+='Volumen_Myocardio,'
        f2_norm=open('HeartValues_ReferenceVolMax_Vol_VolMyo.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax_Vol_VolMyo.csv',np.transpose(res),delimiter=',',header=prop, comments='',fmt='%s')
        f2_norm=open('HeartValues_ReferenceVolMax__Vol_VolMyo_Norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax__Vol_VolMyo_Norm.csv',np.transpose(res_norm),delimiter=',',header=prop, comments='',fmt='%s')

        res=list()
        res_norm=list()
        """Rest of prop"""
        for i in self.normal_Heart_datas.keys(): #Iterate by Propierties
            if not(i=='Volumen' or i=='Volumen Myocardio'):
                for heartPart in self.normal_Heart_datas[i].keys():
                    vals=list()

                    for values in self.normal_Heart_datas[i][heartPart]:
                        vals.append(values[16])
                    for values in self.HCM_Heart_datas[i][heartPart]:
                        vals.append(values[16])

                    res.append(np.array(vals))
                    """Normalize vector"""
                    mx=np.max(vals)
                    mn=np.min(vals)
                    vals_norm=[(i-mn)/(mx-mn) for i in vals]
                    res_norm.append(np.array(vals_norm))

                    prop+=i.replace('[mm]','').replace('[deg]','').replace('[deg/cm]','').replace('[%]','').replace('(','').replace(')','').replace(' ','_')+'_'+heartPart+','
        prop+='Class'

        cl=['Normal']*len(self.normal_Heart_datas['Volumen'])+['Hcm']*len(self.HCM_Heart_datas['Volumen'])
        res.append(cl)
        res_norm.append(cl)

        f2_norm=open('HeartValues_ReferenceVolMax.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax.csv',np.transpose(res),delimiter=',',header=prop, comments='',fmt='%s')
        f2_norm=open('HeartValues_ReferenceVolMax_Norm.csv', mode='x')
        f2_norm.close()
        np.savetxt('HeartValues_ReferenceVolMax_Norm.csv',np.transpose(res_norm),delimiter=',',header=prop, comments='',fmt='%s')


    def plot_Graph(self):

        for par in self.HCM_Heart_datas:
            #print(par)
            os.makedirs(par.replace('/','-'))
            for n,h in zip(self.normal_Heart_datas[par],self.HCM_Heart_datas[par]):
                #print(n,h)
                plt.ion()  # Nos ponemos en modo interactivo
                plt.grid(True)
                plt.subplot(2,1,1)
                plt.suptitle(par+' - Part: '+n)

                rs=signal.resample(np.array(self.normal_Heart_datas[par][n][0]),35)
                i_max=np.argmax(rs)
                plt.plot(rs)
                for j in self.normal_Heart_datas[par][n][1:]:
                    diff=i_max-np.argmax(j)
                    rs=signal.resample(np.array(j[:]),35)
                    if diff >0:
                        rs=np.concatenate((rs[-diff:],rs[:35-diff]))
                    elif diff<0:
                        rs=np.concatenate((rs[abs(diff):],rs[:abs(diff)]))

                    plt.plot(rs)

                plt.xticks(range(35))
                plt.xlabel('Normal', fontsize=9)
                plt.grid(True)

                plt.subplot(2,1,2)
                rs=signal.resample(np.array(self.HCM_Heart_datas[par][n][0]),35)
                i_max=np.argmax(rs)
                plt.plot(rs)
                for k in self.HCM_Heart_datas[par][h][1:]:
                    diff=i_max-np.argmax(k)
                    rs=signal.resample(np.array(k[:]),35)
                    if diff >0:
                        rs=np.concatenate((rs[-diff:],rs[:-diff]))
                    elif diff<0:
                        rs=np.concatenate((rs[abs(diff):],rs[:abs(diff)]))

                    plt.plot(rs)

                plt.xticks(range(35))
                plt.grid(True)
                plt.rc('font', size = 10)
                plt.xlabel('HCM Case', fontsize=9)
                plt.savefig(par.replace('/','-')+'/'+'test_'+n+'.png')
                plt.clf()

hc=heart_cases()
n,h=hc.timepoints_normal_cases_datas(),hc.timepoints_HCM_cases_datas()
#hc.plot_Graph()